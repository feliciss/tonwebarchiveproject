<!DOCTYPE html>
<html>
<body>
  <p align="center">
    <a href="https://feliciss.itcouldbewor.se/tonwebarchivewebclient" target="_blank" rel="noopener noreferrer">
      <img alt="The Open Network" src="https://ton.org/download/ton_symbol.svg" width="60" />
    </a>
  </p>
  <h1 align="center">The Open Network Web Archive Project</h1>
</body>
</html>

👋 Goodbye, The Open Network Web Archive Project.

Sorry, not sorry. This project is no longer being maintained by its original author due to insufficient funds.

Yet, you can still utilize this project by forking it.

## The Network

The Open Network Web Archive Project (the "Project") is hosted on the Clearnet and Darknet, with the domain https://feliciss.itcouldbewor.se/tonwebarchivewebclient for Clearnet and http://webarchive.ton (link breaking) for Darknet. To access TON sites, one should use public entry TON proxies such as [Tonutils-Proxy on GitHub](https://github.com/xssnick/Tonutils-Proxy) or use an appended domain name with .run as the top-level domain.

If one doesn't want to connect to TON proxies due to privacy reasons, etc., one can use an appended .run domain name via Cloudflare. The Project's domain through Cloudflare can be accessed at https://webarchive.ton.run (link breaking).

## The Project

The goal of the Project is to help people archive or migrate their static websites to domains ending in .ton, which can be accessed using TON proxies and circumvent surveillance from the public internet or restrictive networks. The Project's homepage is located at https://feliciss.itcouldbewor.se/tonwebarchivewebclient and http://webarchive.ton (link breaking).

## The Tool

The Project introduces The Open Network Web Archive Journey (the "Journey") as part of the homepage of the Project, which can be accessed at https://feliciss.itcouldbewor.se/tonwebarchivewebclient/journey/intro or http://webarchive.ton/journey/intro (link breaking) via TON Proxies.

Through the Journey, users can upload static websites to a server named The Open Network Web Archive Web Server. The server will process file requests and temporarily store the static website as a TON Torrent on a server that runs a TON Storage Provider to save the torrent files and generate a Bag ID, which is then returned to the users.

Once a Bag ID is generated on a TON Storage Provider, clients can preview their static websites on The Open Network Wayback Machine at http://waybackmachine.ton (link breaking) on the Darknet or https://waybackmachine.ton.run (link breaking) on the Clearnet via a domain-as-a-proxy middleware domain. The Wayback Machine is a non-profit sibling of the Project that hosts the static websites uploaded for free.

Users can then use the generated Bag ID to create a storage request with a TON Storage Provider off-chain on the TON Blockchain. The storage provider will give storage cost to the users based on the rate calculated from the file size and timeframe.

If the users accept the storage cost given by the storage provider, they can use the storage request generated in the previous step to create a storage contract with the storage provider on the TON Blockchain.

Upon creation of the storage contract, a record of the torrent is saved on the storage provider's dataset, and the static websites hosted on The Open Network Wayback Machine would remain available as long as the storage contract has enough Toncoin to cover the storage cost.

## Open Source

All centralized web services and decentralized TON Services are open source.

### Open Network Web Archive Web Client

The Open Network Web Archive Web Client is open source and available at the following `https://` and `.onion` addresses:

HTTPS:

```
https://0xacab.org/feliciss/tonwebarchivewebclient
```

Tor:

```
http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/feliciss/tonwebarchivewebclient
```

### Open Network Web Archive Web Server

The Open Network Web Archive Web Server is open source and available at the following `https://` and `.onion` addresses:

HTTPS:

```
https://0xacab.org/feliciss/tonwebarchivewebserver
```

Tor:

```
http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/feliciss/tonwebarchivewebserver
```

### Open Network Wayback Machine

The Open Network Wayback Machine is open source and available at the following `https://` and `.onion` addresses:

HTTPS:

```
https://0xacab.org/feliciss/tonwaybackmachine
```

Tor:

```
http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/feliciss/tonwaybackmachine
```

## Donations

As a non-commercial project, donations are welcome.

v3R2 wallet address:

```
EQAiDzTZD9obgei7eDHYHUpmJFc3Ttw2VlKk9E6eKNwdaaeE
```

ton:// deeplink:

```
ton://transfer/EQAiDzTZD9obgei7eDHYHUpmJFc3Ttw2VlKk9E6eKNwdaaeE
```
